package com.xyz.trade.report.repo;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.xyz.trade.report.model.TradeEvent;

@Repository
public interface TradeEventRepositry extends CrudRepository<TradeEvent, Long> {
	
	List<TradeEvent> findBySellerPartyAndPremiumCurrency(String sellerParties, String premiumCurrency);

}
