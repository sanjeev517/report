package com.xyz.trade.report.config;

import org.kie.api.KieServices;
import org.kie.api.builder.KieBuilder;
import org.kie.api.builder.KieFileSystem;
import org.kie.api.builder.KieModule;
import org.kie.api.runtime.KieContainer;
import org.kie.internal.io.ResourceFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.event.ContextStartedEvent;
import org.springframework.context.event.EventListener;

import com.xyz.trade.report.service.EventReportingService;

@Configuration
public class ServiceConfiguration {

	private static final Logger log = LoggerFactory.getLogger(ServiceConfiguration.class);
	
	private final KieServices kieServices = KieServices.Factory.get();
	
	@Autowired
	private EventReportingService eventReportingService;
	
    @Bean
    public KieContainer getKieContainer(@Value("${eventFilterRulesFile}") String eventFilterRule) {
        KieFileSystem kieFileSystem = kieServices.newKieFileSystem();
        kieFileSystem.write(ResourceFactory.newFileResource(eventFilterRule));
        KieBuilder kb = kieServices.newKieBuilder(kieFileSystem);
        kb.buildAll();
        KieModule kieModule = kb.getKieModule();
        return kieServices.newKieContainer(kieModule.getReleaseId());
    }
    
    @EventListener
    public void onApplicationEvent(ContextStartedEvent event) {
    	log.info("Loading events after the application boot.");
    	eventReportingService.reload();
    }

    

}