package com.xyz.trade.report.config;

import java.util.List;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Component;

import com.xyz.trade.report.model.TradeEvent;

@Component
@Configuration
@ConfigurationProperties(prefix = "filter")
public class TradeEventFilterConfigProperties {

	private List<TradeEvent> tradeEvents;
	
	public List<TradeEvent> getTradeEvents() {
		return tradeEvents;
	}
	
	public void setTradeEvents(List<TradeEvent> tradeEvents) {
		this.tradeEvents = tradeEvents;
	}
}
