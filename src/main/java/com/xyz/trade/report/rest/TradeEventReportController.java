package com.xyz.trade.report.rest;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.xyz.trade.report.model.TradeEvent;
import com.xyz.trade.report.service.EventReportingService;

@RestController
@RequestMapping("events")
public class TradeEventReportController {

	@Autowired
	private EventReportingService eventReportingService;
	
	
	@GetMapping("/reload")
	public void reloadEventsRequest() {
		eventReportingService.reload();
	}
	
	@GetMapping("/droolBased/report")
	public List<TradeEvent> getTradeEvents() {
		return eventReportingService.generateReport();
	}
	
	@GetMapping("/jpaBased/report")
	public List<TradeEvent> getJpaQueryBasedTradeEvents() {
		return eventReportingService.generateJPABasedReport();
	}
}
