package com.xyz.trade.report.service;

import java.io.File;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathFactory;

import org.springframework.stereotype.Component;
import org.w3c.dom.Document;

import com.xyz.trade.report.exception.TradeEventException;
import com.xyz.trade.report.model.TradeEvent;

@Component
public class EventParser {

	private static DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
	
	public static String BUYER_PARTY_XPATH = "//buyerPartyReference/@href";
	
	public static String SELLER_PARTY_XPATH = "//sellerPartyReference/@href";
	
	public static String P_AMOUNT_XPATH = "//paymentAmount/amount";
	
	public static String P_CURRENCY_XPATH = "//paymentAmount/currency";

	public TradeEvent parse(File file) {
		
		DocumentBuilder db;
		try {
			db = dbf.newDocumentBuilder();
			Document xml = db.parse(file);
			TradeEvent event = new TradeEvent();
			event.setFileName(file.getName());
			// Get XPath
			XPathFactory xpf = XPathFactory.newInstance();
			XPath xpath = xpf.newXPath();

			event.setBuyerParty((String) xpath.evaluate(BUYER_PARTY_XPATH, xml, XPathConstants.STRING));
			event.setSellerParty((String) xpath.evaluate(SELLER_PARTY_XPATH, xml, XPathConstants.STRING));
			event.setPremiumCurrency((String) xpath.evaluate(P_CURRENCY_XPATH, xml, XPathConstants.STRING));
			event.setPremiumAmount((String) xpath.evaluate(P_AMOUNT_XPATH, xml, XPathConstants.STRING)); 
			
			return event;
		} catch (Exception e) {
			throw new TradeEventException("Error while parsing file" + file.getName(), e);
		}
	}

}
