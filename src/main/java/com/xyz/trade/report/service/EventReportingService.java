package com.xyz.trade.report.service;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.kie.api.runtime.KieContainer;
import org.kie.api.runtime.KieSession;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.xyz.trade.report.config.TradeEventFilterConfigProperties;
import com.xyz.trade.report.exception.TradeEventException;
import com.xyz.trade.report.model.TradeEvent;
import com.xyz.trade.report.repo.TradeEventRepositry;

@Service
public class EventReportingService {
	private static final Logger log = LoggerFactory.getLogger(EventReportingService.class);
	
	@Value("${eventsFolder}")
	private String eventFolder;
	
	@Autowired
	private EventParser parser;
	
	@Autowired
	private TradeEventRepositry tradeEventRepo; 
	
	@Autowired
	private KieContainer kieContainer;
	
	@Autowired
	private TradeEventFilterConfigProperties tradeEventsFilter;
	

	public List<TradeEvent> reload(){
		log.info("Events reload request.");
		tradeEventRepo.deleteAll();
		Set<TradeEvent> events = Stream.of(new File(eventFolder).listFiles())
			      .filter(file -> !file.isDirectory())
			      .map(e -> parser.parse(e))
			      .filter(e -> e != null)
			      .collect(Collectors.toSet());
		return (List<TradeEvent>) tradeEventRepo.saveAll(events);
	}
	
	public List<TradeEvent> generateReport() {
		log.info("Report gernation request");
		try {
		List<TradeEvent> events = (List<TradeEvent>) tradeEventRepo.findAll();
		KieSession kieSession = kieContainer.newKieSession();
		for (TradeEvent tradeEvent : events) {
			kieSession.insert(tradeEvent);
			kieSession.fireAllRules();
		}
	    kieSession.dispose();
	    return events.stream().filter(e -> e.isFiltered()).collect(Collectors.toList());
		} catch(Exception ex) {
			log.error("Error while processing events report generation request", ex);
			throw new TradeEventException("Error while processing events report generation request:", ex);
		}
		
	}

	public List<TradeEvent> generateJPABasedReport() {
		log.info("Report gernation based on JPA Request");
		List<TradeEvent> tradeEvents = tradeEventsFilter.getTradeEvents();
		List<TradeEvent> events = new ArrayList<>();
		for (TradeEvent tradeEvent : tradeEvents) {
			events.addAll((List<TradeEvent>) tradeEventRepo.findBySellerPartyAndPremiumCurrency(tradeEvent.getSellerParty(), tradeEvent.getPremiumCurrency()));
		}
		return events.stream().filter(e -> !e.isAnagram()).collect(Collectors.toList());
	}

}
