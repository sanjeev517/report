package com.xyz.trade.report.exception;

public class TradeEventException extends RuntimeException {

	public TradeEventException(String mssg, Exception e) {
		super(mssg, e);
	}
	
	public TradeEventException(String mssg) {
		super(mssg);
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
}
