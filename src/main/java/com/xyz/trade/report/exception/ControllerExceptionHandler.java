package com.xyz.trade.report.exception;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;

@ControllerAdvice
public class ControllerExceptionHandler {
  
  @ExceptionHandler(value = {TradeEventException.class, Exception.class})
  public ResponseEntity<ErrorResponse> resourceNotFoundException(TradeEventException ex, WebRequest request) {
	ErrorResponse message = new ErrorResponse(ex.getMessage(), "FAILED");
    return new ResponseEntity<ErrorResponse>(message, HttpStatus.INTERNAL_SERVER_ERROR);
  }
}