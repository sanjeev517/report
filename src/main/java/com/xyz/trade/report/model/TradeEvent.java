package com.xyz.trade.report.model;

import java.util.Arrays;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;


@Entity
@Table(name = "TRADE_EVENT")
public class TradeEvent {

	@Id
	@GeneratedValue
	private long id;
	
	@Column(name = "FILE_NAME")
	private String fileName;
	
	@Column(name="BUYER_PARTY")
	private String buyerParty;
	
	@Column(name="SELLLER_PARTY")
	private String sellerParty;
	
	@Column(name="PREMIUM_AMONUT")
	private String premiumAmount;
	
	@Column(name="BUYER_CURRENCY")
	private String premiumCurrency;
	
	@Transient
	private boolean filtered;
	
	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public String getBuyerParty() {
		return buyerParty;
	}

	public void setBuyerParty(String buyerParty) {
		this.buyerParty = buyerParty;
	}

	public String getSellerParty() {
		return sellerParty;
	}

	public void setSellerParty(String sellerParty) {
		this.sellerParty = sellerParty;
	}

	public String getPremiumAmount() {
		return premiumAmount;
	}

	public void setPremiumAmount(String premiumAmount) {
		this.premiumAmount = premiumAmount;
	}

	public String getPremiumCurrency() {
		return premiumCurrency;
	}

	public void setPremiumCurrency(String premiumCurrency) {
		this.premiumCurrency = premiumCurrency;
	}

	public boolean isFiltered() {
		return filtered;
	}
	
	public void setFiltered(boolean filtered) {
		this.filtered = filtered;
	}
	
	public boolean isAnagram() {
		 if (buyerParty.length() != sellerParty.length()) {
		        return false;
		    }
		    char[] a1 = buyerParty.toCharArray();
		    char[] a2 = sellerParty.toCharArray();
		    Arrays.sort(a1);
		    Arrays.sort(a2);
		    return Arrays.equals(a1, a2);
	}
}
