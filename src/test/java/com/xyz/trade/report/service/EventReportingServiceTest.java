package com.xyz.trade.report.service;




import java.util.ArrayList;
import java.util.List;

import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.kie.api.runtime.KieContainer;
import org.mockito.InjectMocks;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.util.ReflectionTestUtils;

import com.xyz.trade.report.config.ServiceConfiguration;
import com.xyz.trade.report.config.TradeEventFilterConfigProperties;
import com.xyz.trade.report.model.TradeEvent;
import com.xyz.trade.report.repo.TradeEventRepositry;

@ExtendWith(SpringExtension.class)
@ContextConfiguration(classes={ServiceConfiguration.class, EventReportingService.class, TradeEventFilterConfigProperties.class})
@TestPropertySource(properties = {"eventFilterRulesFile=src/test/resources/eventRules.drl"})
public class EventReportingServiceTest {

	@InjectMocks
	private EventReportingService eventReportingService;
	
	@MockBean
	private EventParser parser;

	@MockBean
	private TradeEventRepositry tradeEventRepo; 
	
	@Autowired
	private KieContainer kieContainer;
	
	@MockBean
	private TradeEventFilterConfigProperties tradeEventsFilter;

	@BeforeEach
    public void setUp() {
        MockitoAnnotations.openMocks(this);
        ReflectionTestUtils.setField(eventReportingService, "eventFolder", "src/test/resources/events");
        ReflectionTestUtils.setField(eventReportingService, "kieContainer", kieContainer);
    }
	
	@Test
	public void reloadTest() {
		Iterable<TradeEvent> list = new ArrayList<>();
		Mockito.when(parser.parse(Mockito.any())).thenReturn(new TradeEvent());
		Mockito.when(tradeEventRepo.saveAll(Mockito.any())).thenReturn(list);
		eventReportingService.reload();
	}
	
	@Test
	public void reloadTest_onLoadFailure() {
		Mockito.when(parser.parse(Mockito.any())).thenReturn(new TradeEvent());
		Mockito.when(tradeEventRepo.saveAll(Mockito.any())).thenThrow(RuntimeException.class);
		
		Assertions.assertThatExceptionOfType(Exception.class).isThrownBy(() -> eventReportingService.reload());
	}
	
	@Test
	public void testRules() {
		List<TradeEvent> events = new ArrayList<TradeEvent>();
		TradeEvent event1 = new TradeEvent();
		event1.setFileName("event");
		event1.setBuyerParty("testBuyer");
		event1.setSellerParty("EMU_BANK");
		event1.setPremiumAmount("100.00");
		event1.setPremiumCurrency("AUD");
		
		events.add(event1);
		
		TradeEvent event2 = new TradeEvent();
		event2.setFileName("event2");
		event2.setBuyerParty("BANK_EMU"); // anagram account
		event2.setSellerParty("EMU_BANK");
		event2.setPremiumAmount("100.00");
		event2.setPremiumCurrency("AUD");
		
		events.add(event2);

		Mockito.when(tradeEventRepo.findAll()).thenReturn(events);
		Iterable<TradeEvent> filtered = eventReportingService.generateReport();
		Assertions.assertThat(filtered).hasSize(1);
	}
	
	@Test
	public void testJPAReport() {
		List<TradeEvent> events = new ArrayList<TradeEvent>();
		TradeEvent event1 = new TradeEvent();
		event1.setFileName("event");
		event1.setBuyerParty("testBuyer");
		event1.setSellerParty("EMU_BANK");
		event1.setPremiumAmount("100.00");
		event1.setPremiumCurrency("AUD");
		
		events.add(event1);
		
		TradeEvent event2 = new TradeEvent();
		event2.setFileName("event2");
		event2.setBuyerParty("BANK_EMU"); // anagram account
		event2.setSellerParty("EMU_BANK");
		event2.setPremiumAmount("100.00");
		event2.setPremiumCurrency("AUD");
		
		events.add(event2);

		Mockito.when(tradeEventRepo.findBySellerPartyAndPremiumCurrency(Mockito.any(), Mockito.any())).thenReturn(events);
		Mockito.when(tradeEventsFilter.getTradeEvents()).thenReturn(events);
		
		Iterable<TradeEvent> filtered = eventReportingService.generateJPABasedReport();
		Assertions.assertThat(filtered).hasSize(2);
	}
	
	
	
	
}
