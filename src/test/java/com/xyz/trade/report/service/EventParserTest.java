package com.xyz.trade.report.service;

import java.io.File;

import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;

import com.xyz.trade.report.model.TradeEvent;

public class EventParserTest {

	@Test
	public void parseTest() {
		EventParser parser = new EventParser();
		TradeEvent event = parser.parse(new File("src/test/resources/events/event.xml"));
		Assertions.assertThat(event).isNotNull();
		Assertions.assertThat(event.getFileName()).isEqualTo("event.xml");
		Assertions.assertThat(event.getBuyerParty()).isEqualTo("RIGHT_BANK");
		Assertions.assertThat(event.getSellerParty()).isEqualTo("EMU_BANK");
		Assertions.assertThat(event.getPremiumCurrency()).isEqualTo("HKD");
		Assertions.assertThat(event.getPremiumAmount()).isEqualTo("100.00");
		
	}
	
}
