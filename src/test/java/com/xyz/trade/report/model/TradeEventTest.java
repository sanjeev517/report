package com.xyz.trade.report.model;

import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;

import com.xyz.trade.report.model.TradeEvent;

public class TradeEventTest {
	
	@Test
	public void testAnagram() {
		TradeEvent event2 = new TradeEvent();
		event2.setFileName("event2");
		event2.setBuyerParty("BANK_EMU"); // anagram 
		event2.setSellerParty("EMU_BANK");
		Assertions.assertThat(event2.isAnagram()).isTrue();
		
		TradeEvent event3 = new TradeEvent();
		event3.setFileName("event3");
		event3.setBuyerParty("BANK_EMU"); //non- anagram 
		event3.setSellerParty("TEST_BANK");
		Assertions.assertThat(event3.isAnagram()).isFalse();
		
	}

}
