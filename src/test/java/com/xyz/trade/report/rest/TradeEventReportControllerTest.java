package com.xyz.trade.report.rest;


import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.skyscreamer.jsonassert.JSONAssert;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import com.xyz.trade.report.exception.TradeEventException;
import com.xyz.trade.report.model.TradeEvent;
import com.xyz.trade.report.service.EventReportingService;

@WebMvcTest(value = TradeEventReportController.class)
@ExtendWith(SpringExtension.class)
public class TradeEventReportControllerTest {

	@Autowired
	private MockMvc mockMvc;
	
	@MockBean
	private EventReportingService eventReportingService;
	
	
	@Test
	public void drool_based_report_success() throws Exception {
		
		List<TradeEvent> events = new ArrayList<>();
		TradeEvent event1 = new TradeEvent();
		event1.setFileName("test1");
		event1.setBuyerParty("testBuyer");
		event1.setSellerParty("testSeller");
		event1.setPremiumAmount("100.00");
		event1.setPremiumCurrency("AUD");
		
		events.add(event1);
		

		Mockito.when(eventReportingService.generateReport()).thenReturn(events);
		
		String expected = "[{\"id\":0,\"fileName\":\"test1\",\"buyerParty\":\"testBuyer\",\"sellerParty\":\"testSeller\",\"premiumAmount\":\"100.00\",\"premiumCurrency\":\"AUD\",\"filtered\":false,\"anagram\":false}]";

		RequestBuilder requestBuilder = MockMvcRequestBuilders.get("/events/droolBased/report/");
		MvcResult result = mockMvc.perform(requestBuilder).andReturn();
		System.out.println(result.getResponse().getContentAsString());
		
		JSONAssert.assertEquals(expected, result.getResponse()
				.getContentAsString(), false);
	}
	
	@Test
	public void report_failure() throws Exception {
		
		List<TradeEvent> events = new ArrayList<>();
		TradeEvent event1 = new TradeEvent();
		event1.setFileName("test1");
		event1.setBuyerParty("testBuyer");
		event1.setSellerParty("testSeller");
		event1.setPremiumAmount("100.00");
		event1.setPremiumCurrency("AUD");
		
		events.add(event1);
		

		Mockito.when(eventReportingService.generateReport()).thenThrow(new TradeEventException("Error while processing"));
		
		String expected = "{\"message\":\"Error while processing\",\"status\":\"FAILED\"}";

		RequestBuilder requestBuilder = MockMvcRequestBuilders.get("/events/droolBased/report/");
		MvcResult result = mockMvc.perform(requestBuilder).andReturn();
		System.out.println(result.getResponse().getContentAsString());
		
		JSONAssert.assertEquals(expected, result.getResponse()
				.getContentAsString(), false);
	}
	
	@Test
	public void jpa_based_report_success() throws Exception {
		
		List<TradeEvent> events = new ArrayList<>();
		TradeEvent event1 = new TradeEvent();
		event1.setFileName("test1");
		event1.setBuyerParty("testBuyer");
		event1.setSellerParty("testSeller");
		event1.setPremiumAmount("100.00");
		event1.setPremiumCurrency("AUD");
		
		events.add(event1);
		

		Mockito.when(eventReportingService.generateJPABasedReport()).thenReturn(events);
		
		String expected = "[{\"id\":0,\"fileName\":\"test1\",\"buyerParty\":\"testBuyer\",\"sellerParty\":\"testSeller\",\"premiumAmount\":\"100.00\",\"premiumCurrency\":\"AUD\",\"filtered\":false,\"anagram\":false}]";

		RequestBuilder requestBuilder = MockMvcRequestBuilders.get("/events/jpaBased/report/");
		MvcResult result = mockMvc.perform(requestBuilder).andReturn();
		System.out.println(result.getResponse().getContentAsString());
		
		JSONAssert.assertEquals(expected, result.getResponse()
				.getContentAsString(), false);
	}
}
