package com.xyz.trade.report;

import java.util.List;

import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.Import;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import com.xyz.trade.report.config.ServiceConfiguration;
import com.xyz.trade.report.model.TradeEvent;
import com.xyz.trade.report.rest.TradeEventReportController;

@ExtendWith(SpringExtension.class)
@Import(ServiceConfiguration.class)
@SpringBootTest
public class IntegrationTests {
 
    @Autowired
    TradeEventReportController tradeEventController;
 
    @Test
    public void testEndToEnd() {
 
    	tradeEventController.reloadEventsRequest();
    	List<TradeEvent> events = tradeEventController.getTradeEvents();

    	Assertions.assertThat(events).isNotEmpty();
    	Assertions.assertThat(events.size()).isEqualTo(4);
    	
    	events = tradeEventController.getJpaQueryBasedTradeEvents();

    	Assertions.assertThat(events).isNotEmpty();
    	Assertions.assertThat(events.size()).isEqualTo(4);
    }
 
 
}