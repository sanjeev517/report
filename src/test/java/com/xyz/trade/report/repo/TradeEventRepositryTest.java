package com.xyz.trade.report.repo;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import com.xyz.trade.report.model.TradeEvent;

@ExtendWith(SpringExtension.class)
@DataJpaTest
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
public class TradeEventRepositryTest {

	@Autowired
	private TradeEventRepositry tradeRepositry;

	@Test
	public void testSaveAll() {

		List<TradeEvent> events = new ArrayList<TradeEvent>();
		TradeEvent event1 = new TradeEvent();
		event1.setFileName("test1");
		event1.setBuyerParty("testBuyer");
		event1.setSellerParty("testSeller");
		event1.setPremiumAmount("100.00");
		event1.setPremiumCurrency("AUD");
		
		events.add(event1);
		tradeRepositry.saveAll(events);
		
		Assertions.assertThat(tradeRepositry.findAll()).isNotEmpty();
	}
	
	@Test
	public void testFindBySellerPartyAndPremiumCurrency() {

		List<TradeEvent> events = new ArrayList<TradeEvent>();
		TradeEvent event1 = new TradeEvent();
		event1.setFileName("test1");
		event1.setBuyerParty("testBuyer");
		event1.setSellerParty("testSeller");
		event1.setPremiumAmount("100.00");
		event1.setPremiumCurrency("AUD");
		events.add(event1);
		
		TradeEvent event2 = new TradeEvent();
		event2.setFileName("test2");
		event2.setBuyerParty("testBuyer2");
		event2.setSellerParty("testSeller2");
		event2.setPremiumAmount("100.00");
		event2.setPremiumCurrency("AUD");
		
		events.add(event2);
		
		TradeEvent event3 = new TradeEvent();
		event3.setFileName("test3");
		event3.setBuyerParty("testBuyer3");
		event3.setSellerParty("testSeller3");
		event3.setPremiumAmount("100.00");
		event3.setPremiumCurrency("USD");
		
		events.add(event3);
		
		tradeRepositry.saveAll(events);
		
		Assertions.assertThat(tradeRepositry.findAll()).hasSize(3);
		
		Assertions.assertThat(tradeRepositry.findBySellerPartyAndPremiumCurrency("testSeller", "AUD")).hasSize(1);
		
	}

}
