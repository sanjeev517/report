### Description
	Trade Reporting Engine: 
	Requirement: A Java program (SpringBoot) that reads a set of XML event files, extracts a set of elements (fields), stores them into DB, filters the events based on a set of criteria, and reports the events in JSON Format.
	Filtering Criteria: (The seller_party is EMU_BANK and the premium_currency is AUD) or (the seller_party is BISON_BANK and the premium_currency is USD) + 	The seller_party and buyer_party must not be anagrams
    Non Functional Requirement: During the design, we need to consider how to extend or add more criteria later without impacting the existing filters.

### Assumptions:
    a. While building the application need to download dependencies libs, so internet connectivity is must if dependencies are locally not available.
    b. The report json fields were not mentioned in the requirements, so assuming only following fields are required. 
    {
    "id": 1,
    "fileName": "event0.xml",
    "buyerParty": "LEFT_BANK",
    "sellerParty": "EMU_BANK",
    "premiumAmount": "100.00",
    "premiumCurrency": "AUD"
  }
   c. Since, the required xsd (fpml-main-5-0.xsd) was not supplied so could not use the JAXB to generate the model classes which could be elegant solution.
   d. Running on default port 8080.
   e. Using default H2 database, with default configurations comes with autoConfiguration
   f. Running with default profile.

### Implemented Solution
    a. Loading events-xmls from folder into DB using Spring JPA, two mechanisim 
        - While booting application
        - Using endpoint. rest endpoint /events/reload
    b. Reports Generation:  The report generation has been implemented in two ways to make to meet the non-functional requirements, making rules externally configurable. Business People could change the logic without changing the actual code.
         - Filtering through the Drools rule engine. The rule file(.drl) is externalized, (can be supplied through command line) so that the filtering logic is decoupled from build.
           Endpoint: /droolBased/report
         - Filtering through JPA query: JPA Filtering criteria is externalized(in application.properties file), so the filtering logic can be updated without much impacting the code.
           Endpoint: /jpaBased/report 
    c. All REST endpoints are exposed through swagger.
         - Local http://localhost:8080/swagger-ui/#/
	
	

### Solution and Test Coverage 
	The application has more then 95% code test coverage . 
![alt text](junit-coverage.jpg?raw=true "Output")



#### Technology stack used
	a. Spring Data with JPA 2.0 Specifications
	b. Swagger API documentation with authentication header.
	c. In memory H2 database.
	d. Drools Rule engine.
	d. Maven for build process.

## Build and Run application

### Prerequisites
	Java 1.8
	Apache Maven (3x)

### Build
	mvn clean install
### Test case execution
	mvn test

### To run application from terminal
	java -jar -DeventsFolder=events -DeventFilterRulesFile=eventRules.drl target/report-0.0.1-SNAPSHOT.jar  (ensure that application is already build using 'mvn clean install' and present working dir is root folder of project)

### REST Soultion using Swagger API
	http://localhost:8080/swagger-ui/#/


Feel free to reach me on contacttosanjeev@gmail.com if you find any issue to run this application.